import React, { useState, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useForm } from 'react-hook-form';
import axios from 'axios';
import MaterialDatatable from "material-datatable";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2)

  },
  delete: {
    backgroundColor: "red"
  }

}));

export default function Libro() {
  const classes = useStyles();

  const { register, handleSubmit,errors,getValues,setValue,reset } = useForm(
    {defaultValues:{codigo:"Codigo a Ingresar",nombre:"Nombre a Ingresar",codigo:"codigo a Ingresar"}});
  const [] = useState(0)
  const [libros, setLibros] = useState([])
  const [autorSeleccionado, setAutorSeleccionado] = useState(0)
  const [accion,setAccion]= useState("Guardar")
  const [idAutor,setIdAutor] = useState(null);
  const [autores, setAutores] = useState([])
  const [] = useState(null);
  

  useEffect(() => {
    cargarLibro();
  }, []);

  useEffect(() => {
    cargarAutor();
  }, []);

  const seleccionar = (item) =>{
    setValue("autor",item._id)
    setIdAutor(item._id)
    }

  const columns1 = [
    {
      name: "Seleccionar",
      options: {
        headerNoWrap: true,
        customBodyRender: (item, tablemeta, update) => {
          return (
            <Button
              variant="contained"
              className="btn-block"
              onClick={() => seleccionar(item)}
            >
              Seleccionar
            </Button>
          );
        },
      },
    },
    {
      name: 'Nombre',
      field: 'nombre'
    }
  
    
  ];

  const columns2 = [
    {
      name: 'Codigo',
      field: 'codigo'
    },
    {
      name: 'Nombre',
      field: 'nombre'
    }
  
    
  ];



  const options1={
    selectableRows: false,
    print: false,
    onlyOneRowCanBeSelected: false,
    textLabels: {
      body: {
        noMatch: "Lo sentimos, no se encuentran registros",
        toolTip: "Sort",
      },
      pagination: {
        next: "Siguiente",
        previous: "Página Anterior",
        rowsPerPage: "Filas por página:",
        displayRows: "de",
      },
    },
    download: false,
    pagination: true,
    rowsPerPage: 5,
    usePaperPlaceholder: true,
    rowsPerPageOptions: [5, 10, 25],
    sortColumnDirection: "desc",
  }

  const options2={
    selectableRows: false,
    print: false,
    onlyOneRowCanBeSelected: false,
    textLabels: {
      body: {
        noMatch: "Lo sentimos, no se encuentran registros",
        toolTip: "Sort",
      },
      pagination: {
        next: "Siguiente",
        previous: "Página Anterior",
        rowsPerPage: "Filas por página:",
        displayRows: "de",
      },
    },
    download: false,
    pagination: true,
    rowsPerPage: 5,
    usePaperPlaceholder: true,
    rowsPerPageOptions: [5, 10, 25],
    sortColumnDirection: "desc",
  }


  const onSubmit = data => {
    console.log(data)
    if(accion=="Guardar"){
      axios
      .post("http://localhost:9000/api/libro", {
            codigo: data.codigo,
            nombre: data.nombre,
            idautor: data.autor
      })
      .then(
        (response) => {
          if (response.status == 200) {
            alert("Registro ok")
            cargarLibro();
            reset();
          }
        },
        (error) => {
          // Swal.fire(
          //   "Error",
          //   "No es posible realizar esta acción: " + error.message,
          //   "error"
          // );
        }
      )
      .catch((error) => {
        // Swal.fire(
        //   "Error",
        //   "No cuenta con los permisos suficientes para realizar esta acción",
        //   "error"
        // );
        console.log(error);
      });
    }

  }

  const ModificaAutorSeleccionado = (event) => {
    setAutorSeleccionado(event.target.value);
  };

  const cargarLibro = async () => {
    // const { data } = await axios.get('/api/zona/listar');

    const { data } = await axios.get("http://localhost:9000/api/libro");
    
    setLibros(data.libroConAutor);


  };

  const cargarAutor = async () => {
    // const { data } = await axios.get('/api/zona/listar');

    const { data } = await axios.get("http://localhost:9000/api/autor");
    
    setAutores(data.autor);


  };
  
  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <div className={classes.paper}>

 
        <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="codigo"
                name="codigo"
                variant="outlined"
                required
                fullWidth
                id="codigo"
                label="codigo"
                autoFocus
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="nombre"
                label="Nombre"
                name="nombre"
                autoComplete="nombre"
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="autor"
                label="autor"
                name="autor"
                autoComplete="autor"
                inputRef={register}
              />
            </Grid>
            <Grid container spacing={1}>
            <MaterialDatatable
        
              title={"Autores"}
              data={autores}
              columns={columns1}
              options={options1}
            />
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            {accion}
          </Button>

          <Grid container spacing={1}>
            <MaterialDatatable
        
              title={"Libros"}
              data={libros}
              columns={columns2}
              options={options2}
            />
          </Grid>
          </Grid>
        
        </form>


      </div>

    </Container>
  );
}