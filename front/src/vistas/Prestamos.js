import React, { useState,useEffect} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useForm } from 'react-hook-form';
import axios from 'axios';
import MaterialDatatable from "material-datatable";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  
  },
  delete : {
    backgroundColor:"red"
  }

}));

export default function Prestamo() {
  const classes = useStyles();
  const fecha = new Date();
  const fechaActual = (fecha.getFullYear()+"-"+(fecha.getMonth()+1)+"-"+fecha.getDate());

  const { register, handleSubmit,errors,getValues,setValue,reset } = useForm(
    {defaultValues:{ fecha: fecha.getFullYear()+"-"+(fecha.getMonth()+1)+"-"+fecha.getDate()} });
  const [] = useState(0)
  const [libros, setLibros] = useState([])
  const [libroSeleccionado, setLibroSeleccionado] = useState(0)
  const [personaSeleccionada, setPersonaSeleccionado] = useState(0)
  const [accion] = useState("Guardar")
  //const [prestamos,setPrestamos] = useState(null);
  const [personas, setPersonas] = useState([])
  const [] = useState(null);
  const [idPersona,setIdPersona] = useState(null);
  const [idLibro,setIdLibro] = useState(null);

  useEffect(() => {
    cargarLibro();
  }, []);

  useEffect(() => {
    cargarPersona();
  }, []);

  /*useEffect(() => {
    cargarPrestamo();
  }, []);*/

  const ModificaPersonaSeleccionado = (event) => {
    setPersonaSeleccionado(event.target.value);
  };
  const ModificaLibroSeleccionado = (event) => {
      setLibroSeleccionado(event.target.value);
    };

  const seleccionar1 = (item) =>{
    setValue("persona",item._id)
    setIdPersona(item._id)
    }

    const seleccionar2 = (item) =>{
      setValue("libro",item._id)
      setIdLibro(item._id)
      }
  

  const columns = [
    {
      name: "Seleccionar",
      options: {
        headerNoWrap: true,
        customBodyRender: (item, tablemeta, update) => {
          return (
            <Button
              variant="contained"
              className="btn-block"
              onClick={() => seleccionar1(item)}
            >
              Seleccionar
            </Button>
          );
        },
      },
    },
    {
      name: 'Rut',
      field: 'rut'
    },
    {
      name: 'Nombre',
      field: 'nombre'
    }
  
    
  ];

  const columns2 = [
    {
      name: "Seleccionar",
      options: {
        headerNoWrap: true,
        customBodyRender: (item, tablemeta, update) => {
          return (
            <Button
              variant="contained"
              className="btn-block"
              onClick={() => seleccionar2(item)}
            >
              Seleccionar
            </Button>
          );
        },
      },
    },
    {
      name: 'Nombre',
      field: 'nombre'
    }
  
    
  ];

 /* const columns3 = [
    {
      name: 'Persona',
      field: '_id'
    },
    {
      name: 'Libro',
      field: '_id'
    },
    {
      name: 'Fecha',
      field: 'fecha'
    }

  
    
  ];*/


  const options={
    selectableRows: false,
    print: false,
    onlyOneRowCanBeSelected: false,
    textLabels: {
      body: {
        noMatch: "Lo sentimos, no se encuentran registros",
        toolTip: "Sort",
      },
      pagination: {
        next: "Siguiente",
        previous: "Página Anterior",
        rowsPerPage: "Filas por página:",
        displayRows: "de",
      },
    },
    download: false,
    pagination: true,
    rowsPerPage: 5,
    usePaperPlaceholder: true,
    rowsPerPageOptions: [5, 10, 25],
    sortColumnDirection: "desc",
  }


  const options2={
    selectableRows: false,
    print: false,
    onlyOneRowCanBeSelected: false,
    textLabels: {
      body: {
        noMatch: "Lo sentimos, no se encuentran registros",
        toolTip: "Sort",
      },
      pagination: {
        next: "Siguiente",
        previous: "Página Anterior",
        rowsPerPage: "Filas por página:",
        displayRows: "de",
      },
    },
    download: false,
    pagination: true,
    rowsPerPage: 5,
    usePaperPlaceholder: true,
    rowsPerPageOptions: [5, 10, 25],
    sortColumnDirection: "desc",
  }

  const options3={
    selectableRows: false,
    print: false,
    onlyOneRowCanBeSelected: false,
    textLabels: {
      body: {
        noMatch: "Lo sentimos, no se encuentran registros",
        toolTip: "Sort",
      },
      pagination: {
        next: "Siguiente",
        previous: "Página Anterior",
        rowsPerPage: "Filas por página:",
        displayRows: "de",
      },
    },
    download: false,
    pagination: true,
    rowsPerPage: 5,
    usePaperPlaceholder: true,
    rowsPerPageOptions: [5, 10, 25],
    sortColumnDirection: "desc",
  }
  const onSubmit = data => {
    console.log(data)
    if(accion=="Guardar"){
      axios
      .post("http://localhost:9000/api/prestamo", {
            idPersona: data.persona,
            libro: data.libro,
            fecha: fechaActual
            
      })
      .then(
        (response) => {
          if (response.status == 200) {
            alert("Registro ok")
            //cargarPrestamo();
            reset();
          }
        },
        (error) => {
          // Swal.fire(
          //   "Error",
          //   "No es posible realizar esta acción: " + error.message,
          //   "error"
          // );
        }
      )
      .catch((error) => {
        // Swal.fire(
        //   "Error",
        //   "No cuenta con los permisos suficientes para realizar esta acción",
        //   "error"
        // );
        console.log(error);
      });
    }

  }

 /* const cargarPrestamo = async () => {
    // const { data } = await axios.get('/api/zona/listar');

    const { data } = await axios.get("http://localhost:9000/api/prestamo");
    
    setPrestamos(data.prestamo);


  };*/

  const cargarLibro = async () => {
    // const { data } = await axios.get('/api/zona/listar');

    const { data } = await axios.get("http://localhost:9000/api/libro");
    
    setLibros(data.libroConAutor);


  };

  const cargarPersona = async () => {
    // const { data } = await axios.get('/api/zona/listar');

    const { data } = await axios.get("http://localhost:9000/api/personas");
    
    setPersonas(data.persona);


  };
  
  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <div className={classes.paper}>

 
        <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="persona"
                name="persona"
                variant="outlined"
                required
                fullWidth
                id="personaid"
                label="persona"
                autoFocus
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="libro"
                label="libro"
                name="libro"
                autoComplete="libro"
                inputRef={register}
              />
            </Grid>
            <Grid>
            <MaterialDatatable
        
              title={"Persona"}
              data={personas}
              columns={columns}
              options={options}
            />
          </Grid>
          <Grid>
            <MaterialDatatable
        
              title={"Libros"}
              data={libros}
              columns={columns2}
              options={options2}
            />
          </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            {accion}
          </Button>


  
        
        </form>


      </div>

    </Container>
  );
}